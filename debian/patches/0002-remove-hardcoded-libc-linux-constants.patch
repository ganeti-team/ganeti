From: Apollon Oikonomopoulos <apoikos@debian.org>
Date: Mon, 28 Jan 2019 11:10:24 +0200
Subject: Do not hardcode arch-dependent libc/linux constants

commit 1abcb876d279f698b0fafe723feac22540d145f9
Author: Apollon Oikonomopoulos <apoikos@debian.org>
Date:   Mon Jan 28 11:00:13 2019 +0200

    utils.mlock: do not use hardcoded mlockall(2) flags

    Switch to using the build-time detected flags from constants.

    Signed-off-by: Apollon Oikonomopoulos <apoikos@debian.org>

commit b273f537019249ce693f8df78640ff5c6c6bdf4c
Author: Apollon Oikonomopoulos <apoikos@debian.org>
Date:   Mon Jan 28 10:59:31 2019 +0200

    kvm.netdev: do not use hardcoded ioctl values

    Switch to using the build-time detected values from constants.

    Signed-off-by: Apollon Oikonomopoulos <apoikos@debian.org>

commit 681b23e163fc7d1fd1c9a88906834c67b9f0bf2e
Author: Apollon Oikonomopoulos <apoikos@debian.org>
Date:   Mon Jan 28 10:51:32 2019 +0200

    Derive arch-dependent constant values from libc/linux headers

    We are currently hardcoding some C constants in our Python code in two
    places: the mlockall(2) flags (used via ctypes), and the TUN/TAP driver
    ioctls. These constants are actually architecture-dependent and should
    be derived at build time.

    Use hsc2py to append these definitions to src/AutoConf.hs, and have them
    propagate to Ganeti.Constants (and from there lib/_constants.py). A
    follow-up commit will replace the current constants with the derived
    ones.

    Signed-off-by: Apollon Oikonomopoulos <apoikos@debian.org>
---
 Makefile.am                                         |  7 +++++--
 autotools/HeaderConstants.hsc                       | 20 ++++++++++++++++++++
 lib/hypervisor/hv_kvm/netdev.py                     |  8 +++-----
 lib/utils/mlock.py                                  |  8 ++------
 src/Ganeti/Constants.hs                             | 17 +++++++++++++++++
 test/py/legacy/ganeti.hypervisor.hv_kvm_unittest.py |  2 +-
 6 files changed, 48 insertions(+), 14 deletions(-)
 create mode 100644 autotools/HeaderConstants.hsc

diff --git a/Makefile.am b/Makefile.am
index 54331bb..0d75ca2 100644
--- a/Makefile.am
+++ b/Makefile.am
@@ -2339,6 +2339,9 @@ src/Ganeti/Hs2Py/ListConstants.hs: src/Ganeti/Hs2Py/ListConstants.hs.in \
 	m4 -DPY_CONSTANT_NAMES="$$NAMES" \
 		$(abs_top_srcdir)/src/Ganeti/Hs2Py/ListConstants.hs.in > $@
 
+autotools/HeaderConstants.hs: autotools/HeaderConstants.hsc | stamp-directories
+	hsc2hs -o $@ $<
+
 test/hs/Test/Ganeti/TestImports.hs: test/hs/Test/Ganeti/TestImports.hs.in \
 	$(built_base_sources)
 	set -e; \
@@ -2356,7 +2359,7 @@ lib/_constants.py: Makefile $(HS2PY_PROG) lib/_constants.py.in | stamp-directori
 
 lib/constants.py: lib/_constants.py
 
-src/AutoConf.hs: Makefile src/AutoConf.hs.in $(PRINT_PY_CONSTANTS) \
+src/AutoConf.hs: Makefile src/AutoConf.hs.in autotools/HeaderConstants.hs $(PRINT_PY_CONSTANTS) \
 	       | $(built_base_sources)
 	@echo "m4 ... >" $@
 	@m4 -DPACKAGE_VERSION="$(PACKAGE_VERSION)" \
@@ -2433,7 +2436,7 @@ src/AutoConf.hs: Makefile src/AutoConf.hs.in $(PRINT_PY_CONSTANTS) \
 			    done)" \
 	    -DAF_INET4="$$(PYTHONPATH=. $(PYTHON) $(PRINT_PY_CONSTANTS) AF_INET4)" \
 	    -DAF_INET6="$$(PYTHONPATH=. $(PYTHON) $(PRINT_PY_CONSTANTS) AF_INET6)" \
-	$(abs_top_srcdir)/src/AutoConf.hs.in > $@
+	$(abs_top_srcdir)/src/AutoConf.hs.in $(abs_top_srcdir)/autotools/HeaderConstants.hs > $@
 
 lib/_vcsversion.py: Makefile vcs-version | stamp-directories
 	set -e; \
diff --git a/autotools/HeaderConstants.hsc b/autotools/HeaderConstants.hsc
new file mode 100644
index 0000000..82d56d5
--- /dev/null
+++ b/autotools/HeaderConstants.hsc
@@ -0,0 +1,20 @@
+#include <sys/mman.h>
+#include <sys/ioctl.h>
+#include <linux/if_tun.h>
+
+-- mlockall(2) constants
+mclCurrent :: Int
+mclCurrent = #const MCL_CURRENT
+
+mclFuture :: Int
+mclFuture = #const MCL_FUTURE
+
+-- TUN/TAP interface ioctls
+tungetiff :: Integer
+tungetiff = #const TUNGETIFF
+
+tunsetiff :: Integer
+tunsetiff = #const TUNSETIFF
+
+tungetfeatures :: Integer
+tungetfeatures = #const TUNGETFEATURES
diff --git a/lib/hypervisor/hv_kvm/netdev.py b/lib/hypervisor/hv_kvm/netdev.py
index 08f4523..1938c31 100644
--- a/lib/hypervisor/hv_kvm/netdev.py
+++ b/lib/hypervisor/hv_kvm/netdev.py
@@ -37,15 +37,13 @@ import logging
 import struct
 import fcntl
 
+from ganeti import constants
 from ganeti import errors
 
 
 # TUN/TAP driver constants, taken from <linux/if_tun.h>
 # They are architecture-independent and already hardcoded in qemu-kvm source,
 # so we can safely include them here.
-TUNSETIFF = 0x400454ca
-TUNGETIFF = 0x800454d2
-TUNGETFEATURES = 0x800454cf
 IFF_TAP = 0x0002
 IFF_NO_PI = 0x1000
 IFF_ONE_QUEUE = 0x2000
@@ -61,7 +59,7 @@ def _GetTunFeatures(fd, _ioctl=fcntl.ioctl):
   """
   req = struct.pack("I", 0)
   try:
-    buf = _ioctl(fd, TUNGETFEATURES, req)
+    buf = _ioctl(fd, constants.TUNGETFEATURES, req)
   except EnvironmentError as err:
     logging.warning("ioctl(TUNGETFEATURES) failed: %s", err)
     return None
@@ -173,7 +171,7 @@ def OpenTap(name="", features=None):
     ifr = struct.pack("16sh", ifreq_name, flags)
 
     try:
-      res = fcntl.ioctl(tapfd, TUNSETIFF, ifr)
+      res = fcntl.ioctl(tapfd, constants.TUNSETIFF, ifr)
     except EnvironmentError as err:
       raise errors.HypervisorError("Failed to allocate a new TAP device: %s" %
                                    err)
diff --git a/lib/utils/mlock.py b/lib/utils/mlock.py
index 837d716..78ea916 100644
--- a/lib/utils/mlock.py
+++ b/lib/utils/mlock.py
@@ -34,6 +34,7 @@
 import os
 import logging
 
+from ganeti import constants
 from ganeti import errors
 
 try:
@@ -43,11 +44,6 @@ except ImportError:
   ctypes = None
 
 
-# Flags for mlockall(2) (from bits/mman.h)
-_MCL_CURRENT = 1
-_MCL_FUTURE = 2
-
-
 def Mlockall(_ctypes=ctypes):
   """Lock current process' virtual address space into RAM.
 
@@ -77,7 +73,7 @@ def Mlockall(_ctypes=ctypes):
   # pylint: disable=W0212
   libc.__errno_location.restype = _ctypes.POINTER(_ctypes.c_int)
 
-  if libc.mlockall(_MCL_CURRENT | _MCL_FUTURE):
+  if libc.mlockall(constants.MCL_CURRENT | constants.MCL_FUTURE):
     # pylint: disable=W0212
     logging.error("Cannot set memory lock: %s",
                   os.strerror(libc.__errno_location().contents.value))
diff --git a/src/Ganeti/Constants.hs b/src/Ganeti/Constants.hs
index c0a0834..157929e 100644
--- a/src/Ganeti/Constants.hs
+++ b/src/Ganeti/Constants.hs
@@ -5533,3 +5533,20 @@ cliWfjcFrequency = 20
 -- | Default 'WaitForJobChange' timeout in seconds
 defaultWfjcTimeout :: Int
 defaultWfjcTimeout = 60
+
+-- | Arch-dependent mlock(2) flags
+mclCurrent :: Int
+mclCurrent = AutoConf.mclCurrent
+
+mclFuture :: Int
+mclFuture = AutoConf.mclFuture
+
+-- | Arch-dependent TUN ioctl(2) values
+tunsetiff :: Integer
+tunsetiff = AutoConf.tunsetiff
+
+tungetiff :: Integer
+tungetiff = AutoConf.tungetiff
+
+tungetfeatures :: Integer
+tungetfeatures = AutoConf.tungetfeatures
diff --git a/test/py/legacy/ganeti.hypervisor.hv_kvm_unittest.py b/test/py/legacy/ganeti.hypervisor.hv_kvm_unittest.py
index 9111bf3..040a8b9 100755
--- a/test/py/legacy/ganeti.hypervisor.hv_kvm_unittest.py
+++ b/test/py/legacy/ganeti.hypervisor.hv_kvm_unittest.py
@@ -653,7 +653,7 @@ class TestGetTunFeatures(unittest.TestCase):
     self.assertTrue(result is None)
 
   def _FakeIoctl(self, features, fd, request, buf):
-    self.assertEqual(request, netdev.TUNGETFEATURES)
+    self.assertEqual(request, constants.TUNGETFEATURES)
 
     (reqno, ) = struct.unpack("I", buf)
     self.assertEqual(reqno, 0)
